#ifndef S5LOO_CIRCLE_HPP
#define S5LOO_CIRCLE_HPP
#include <tuple>
#include "color.hpp"
#include "SFML/Graphics.hpp"


namespace s5loo {
	class Window;

	class Circle {
	private:
		double x_, y_, sx_, sy_, radius_;
		Color color_;

	public:
		Circle(double x, 
				double y, 
				double speedX, 
				double speedY, 
				double radius, 
				Color color)
		: x_{x}, y_{y}, sx_{speedX}, sy_{speedY}, radius_{radius}, color_{color} {}

		Circle(const Circle&) = default;
		Circle(Circle&&) = default;
		Circle& operator=(const Circle&) = default;
		Circle& operator=(Circle&&) = default;
		~Circle() = default;

		/*-------------------*/

		std::tuple<double, double>
		position() const { return {x_, y_}; }

		/*-------------------*/

		std::tuple<double, double>
		speed() const { return {sx_, sy_}; }

		/*-------------------*/
		double
		radius() const { return radius_; }

		Color
		color() const { return color_; }

		void move(const Window &win, double dt);

	}; // end of class Circle


	void
	draw (const Circle &c, sf::RenderWindow& win);

} //end of namespace s5loo


#endif