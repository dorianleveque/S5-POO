#include <iostream>
#include <random>
#include "moduleArray.hpp"

namespace s5loo {

	void fillArray(str15 &words) {
		//words={"hi", "hello", "anna", "kayak", "21:12", "goodbye"};
		std::default_random_engine rndGen	{std::random_device{}()};
		std::uniform_int_distribution<int> lengthDistrib	{3, 6};
		std::uniform_int_distribution<int> charDistrib	{'a', 'e'};

		for (auto &s: words) {
			int wordSize = lengthDistrib(rndGen);
			for (int i=0; i<wordSize; i++) {
				s += (char) charDistrib(rndGen);
			}
		}
	}

	void printArray(const str15 &words) {
		std::cout << "{ ";
		for (const auto &s: words) {
			std::cout << s << ' ';
		}
		std::cout << "}\n";
	}

	bool isPalindrome(std::string s) {
		int begin = 0;
		int end = (int) s.size()-1;
		while (begin < end) {
			if (s[begin++] != s[end--]) {
				return false;
			}
		}
		return true;
	}
}