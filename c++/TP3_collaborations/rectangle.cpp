#include "rectangle.hpp"
#include "window.hpp"
namespace s5loo {

	void Rectangle::move(const Window &win, double dt) {
		angle(angle() + angularSpeed_*dt);
		
		auto [win_width, win_height] = win.size();
		x_ += sx_ * dt;
		y_ += sy_ * dt;
		
		if (x_ < 0) {
			//x_ = 0;
			sx_= -sx_;//0;
		}
		
		if (x_ + width_ >= win_width) {
			//x_ = width-1-2*radius_;
			sx_= -sx_;//0;
		}
		
		if (y_ < 0) {
			//y_ = 0; 
			sy_= -sy_;//0;
		}
		
		if (y_ + height_ >= win_height) { 
			//y_ = height-1-2*radius_; 
			sy_= -sy_;//0;
		}
	}

	void draw(const Rectangle& rect, sf::RenderWindow& win) {
		auto [x, y] = rect.position();
		auto [rectWidth, rectHeight] = rect.size();
		Color rectColor = rect.color();
		double angle = rect.angle();
		
		sf::RectangleShape s{sf::Vector2f{(float)rectWidth, (float)rectHeight}};
		s.setFillColor(sf::Color{rectColor[0], rectColor[1], rectColor[2]});
		s.setPosition((float)x, (float)y);
		s.setRotation((float)angle);		
		win.draw(s);
	}

} //end of namespace s5loo