#include <iostream>
#include "moduleArray.hpp"
#include "moduleVector.hpp"

void test_arrayFunctions() {
	std::cout << "\n-----" << __func__ << "() -----\n";
	s5loo::str15 words;
	s5loo::fillArray(words);
	s5loo::printArray(words);

	std::cout << "Word 2 is a Palindrome ? \t "
				 << (s5loo::isPalindrome(words[2]) ? "YES" : "NO")
				 << '\n';

	std::cout << "And word 1 ? \t "
				 << (s5loo::isPalindrome(words[1]) ? "YES" : "NO")
				 << '\n';
}


void test_vectorFunctions(const char* arg) {
	std::cout << "\n-----" << __func__ << "() -----\n";

	int count;
	try {
		count = std::stoi(arg);
	}
	catch (const std::exception &e) {
		std::cerr << std::string(__func__) + "must receive an integer\n";
		std::exit(0);
	}

	s5loo::intvec vector {s5loo::makeVector(count)};
	s5loo::printVector(vector);
	std::cout << "Nombre de valeur pair dans la liste: " << s5loo::numEven(vector) << '\n';

	auto [average, ecartType, mediane] = s5loo::dataVector(vector);
	std::cout << "moyenne:\t" << average << '\n'
				 << "écart-type:\t" << ecartType << '\n'
				 << "médiane:\t" << mediane <<'\n';
	
}

int main(int argc, char** argv) {
	std::cout << "\n----- " << __func__ << "() -----\n";
	std::cout << "argc: " << argc << '\n'
				 << "argv: ";
				for (int i=0; i<argc; i++) std::cout << argv[i] << ' ';
				 std::cout << '\n';
	
	switch(argv[1]) {
		case "array": test_arrayFunctions(); break;
		case "vector": 
			if (argc < 3) {
				std::cerr << "To run the vector test select the number N of integers to add:\n"
				<< "./prog vector N\n";
				std::exit(0); 
			}
			try {
				test_vectorFunctions(argv[2]);
			}
			catch (const std::exception &e) {
				std::cerr << "Invalid argument: " << e.what() << '\n';
			}
	}
	test_arrayFunctions();
	test_vectorFunctions();
	return 0;
}