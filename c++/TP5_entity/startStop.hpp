#ifndef S5LOO_STARTSTOP_HPP
#define S5LOO_STARTSTOP_HPP

#include <random>
#include "behavior.hpp"

namespace s5loo {

	class StartStop: public Behavior {
	
	private:
		std::default_random_engine rndGen_{std::random_device{}()};
		std::uniform_real_distribution<double> speedDistr_{0, 50};
		std::uniform_real_distribution<double> angularDistr_{-30, 30};	

	public:
		StartStop() = default;

		void click(Entity &e, const Window &win, [[maybe_unused]] int xMouseClic, [[maybe_unused]] int yMouseClic) override;

	}; // end of class StartStop

} // end of namespace s5loo

#endif