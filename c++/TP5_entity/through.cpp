#include "through.hpp"
#include "entity.hpp"
#include "window.hpp"

namespace s5loo {

	void Through::animate(Entity& e, const Window &win, double dt) {
		const auto [win_width, win_height] = win.size();
		auto [x, y] = e.position();
		
		if (x > win_width) x = 0.0;
		if (x < 0.0) x = win_width;
		if (y > win_height) y = 0.0;
		if (y < 0.0) y = win_height;

		position(e, x, y);
	}

} // end of namespace s5loo