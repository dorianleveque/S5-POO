#include "entity.hpp"
#include "window.hpp"

namespace s5loo {

	void Entity::click(const Window &win, int xMouseClick, int yMouseClick) {
		const auto [x, y] = position();
		const double dx = (double) xMouseClick - x;
		const double dy = (double) yMouseClick - y;

		const double radius = boundingSphere();

		if (dx*dx + dy*dy <= radius*radius) {
			// dans la sphère englobante
			for (auto &b: behaviors_) {
				b -> click(*this, win, xMouseClick, yMouseClick);
			}
		}
	}

} // end of namespace s5loo