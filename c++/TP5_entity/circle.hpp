#ifndef S5LOO_CIRCLE_HPP
#define S5LOO_CIRCLE_HPP
#include <tuple>
#include "SFML/Graphics.hpp"
#include "shape.hpp"
#include "entity.hpp"


namespace s5loo {

	class Circle : public Shape {
	private:
		double radius_;

	public:
		Circle(double radius)
		: radius_{radius} {}

		double radius() const {
			return radius_;
		}
		
		void draw(Entity &e, sf::RenderWindow& win) const override;
		
		double boundingSphere() const override {
			return radius_;
		}
		
	}; // end of class Circle
} //end of namespace s5loo


#endif