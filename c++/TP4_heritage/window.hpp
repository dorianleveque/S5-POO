#ifndef S5LOO_WINDOW_HPP
#define S5LOO_WINDOW_HPP 1

#include <vector>
#include <tuple>
#include "SFML/Graphics.hpp"
#include "shape.hpp"

namespace s5loo {

  class Window {
  
  private :
    std::string name_;
    double width_, height_;
    sf::RenderWindow win_;
    std::vector<std::unique_ptr<Shape>> shapes_;

  public :
    Window(std::string n="Window", double w=700, double h=500);
      
    /* Special functions -- copy constructor and copy assignement are deleted */
    Window(const Window&) = delete;
    Window(Window&&) = default;
    Window& operator=(const Window&) = delete;
    Window& operator=(Window&&) = default;
    virtual ~Window() = default;

    std::tuple<double,double> size() const;
    void display(void);
    //void addCircle(Circle c);
    //void addRectangle(Rectangle r);
    void addShape(std::unique_ptr<Shape> s);
    void drawAll();
    void moveAll(double dt);
    void clickAll(int xMouseClicCrd, int yMouseClicCrd);

  };  // class Window

  /* inline member functions */

  inline
  std::tuple<double,double> Window::size() const { return {width_, height_}; }

  inline
  void Window::addShape(std::unique_ptr<Shape> s) {
    shapes_.emplace_back(std::move(s));
  }

  inline
  void Window::drawAll() {
    for (const auto &shape: shapes_) {
      shape->draw(win_);
    }
  }

  inline
  void Window::moveAll(double dt) {
    for (auto &shape: shapes_) {
      shape->move(*this, dt);
    }
  }

  inline
  void Window::clickAll(int xMouseClicCrd, int yMouseClicCrd) {
    for (auto &shape: shapes_) {
      shape->click(*this, xMouseClicCrd, yMouseClicCrd);
    }
  }

  /* global functions déclaration */

  double // seconds since 1970/01/01 00:00:00 UTC (avec des décimales allant jusqu'aux microsecondes)
  getTime();


}  // namespace s5loo

#endif
