# -*-coding:utf-8 -*
class Homme:
	"""
		###################################
		##
		##		 Class Mammifere
		##
		## auteur: Dorian
		## version: 0.0
		###################################
	
	== Description:
		Permet de contenir un ensemble de caractéristiques propre à ce type d'animal (nom, âge, nombre de dents)
	
	== Appel class:
		Mammifere(nom, age, nb_dent)
		| @Params:
		| - nom (string) [undefined]
		| - age (number) [0]
		| - nb_dents (number) [0]
		| @Return None

	"""
	def __init__(self, nom = 'undefined'):
		self._nom = nom
	
	def __del__(self):
		print 'Destruction de Homme'

	def saluer(self):
		print "Bonjour, je suis "+str(self._nom)

class Femme:
	def __init__(self, nom = 'undefined'):
		self._nom = nom

	def __del__(self):
		print('Destruction de Femme')

	def saluera(self):
		print "Bonjour, je m'appelle "+str(self._nom)

class Humain(Homme, Femme):
	def __init__(self):
		Homme.__init__(self)
		Femme.__init__(self)

	def __del__(self):
		print('Destruction de Humain')

	def bonjour(self):
		print "Ta gueule, "+str(self._nom)

homme = Homme('Jarod')
femme = Femme('Jarette')
humain = Humain()

homme.saluer()
femme.saluera()
humain.bonjour()