#ifndef MODULE_ARRAY_HPP
#define MODULE_ARRAY_HPP
#include <string>
#include <array>

namespace s5loo {
	
	using str15 = std::array<std::string, 15>;

	void fillArray(str15 &words);

	void printArray(const str15 &words);

	bool isPalindrome(std::string);

}

#endif