#include "bounce.hpp"
#include "entity.hpp"
#include "window.hpp"

namespace s5loo {

	void Bounce::animate(Entity& e, const Window &win, double dt) {
		const auto [win_width, win_height] = win.size();
		auto [x, y] = e.position();
		auto [sx, sy] = e.speed();
		double bound = e.boundingSphere();

		/*x += sx * dt;
		y += sy * dt;*/

		if (x - bound < 0) {
			x = bound;
			sx = -sx;
		}
		
		if (x + bound >= win_width) {
			x = win_width-bound;
			sx = -sx;
		}
		
		if (y - bound < 0) {
			y = bound;
			sy = -sy;
		}
		
		if (y + bound >= win_height) { 
			y = win_height-bound; 
			sy = -sy;
		}

		speed(e, sx, sy);
		position(e, x, y);
	}

} // end of namespace s5loo