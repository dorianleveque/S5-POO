#include "motion.hpp"
#include "entity.hpp"
#include "window.hpp"

namespace s5loo {

	void
	Motion::animate(Entity& e, [[maybe_unused]] const Window &win, double dt) {
		auto [x, y] = e.position();
		auto [sx, sy] = e.speed();

		x += sx * dt;
		y += sy * dt;

		angle(e, e.angle() + e.angularSpeed() * dt);
		position(e, x, y);
		speed(e, sx, sy);
	}
}