#include <random>
#include <fstream>
#include "image.hpp"

namespace s5loo {
	Image::Image(std::string n, int w, int h, bool random)
	: name_ {n}, width_ {w}, height_ {h}, pixels_ {} {
		
		int size = width_*height_;
		pixels_.reserve(size);

		if (random) {
			std::default_random_engine rndGen { std::random_device {} ()};
			std::uniform_int_distribution<int> component {0, 255};

			for (int i=0; i<size; i++) {
				uchar r = uchar(component(rndGen));
				uchar g = uchar(component(rndGen));
				uchar b = uchar(component(rndGen));
				pixels_.emplace_back(r,g,b);
			}
		}
		else {
			pixels_.resize(size);
		}
	}

	Image::Image(std::string path)
	: name_ {imageName(path)}, width_ {}, height_ {}, pixels_ {} {
		std::ifstream input(path);
		if (!input) {
			throw std::runtime_error("cannot read from file " + path);
		}
		std::string P3;
		int i;
		input >> P3 >> width_ >> height_ >> i;

		if((P3!="P3")||(i!=255)) {
    		throw std::runtime_error("bad header in " + name_);
		}

		for(int size=width_*height_, i=0; i<size; ++i) {
			int r, g, b;
			input >> r >> g >> b;
			pixels_.emplace_back(uchar(r),uchar(g),uchar(b));
		}
	} 

	std::string imageName(std::string path) {
		std::string name;
		if(auto pos_point = path.find_last_of('.'); pos_point!=path.npos)     path.resize(pos_point);
		if(auto pos_slash = path.find_last_of('/'); pos_slash!=path.npos)     name = path.substr(pos_slash+1);
		return name;
	}

	std::ostream& operator<< (std::ostream& os, const Image& img) {
		os << "nom: " 	<< img.name() << '\n'
			<< "largeur: "<< img.width() << '\n'
			<< "hauteur: "<< img.height() << '\n'
			<< "liste des pixels: \n";

		for (int i=0; i<size(img); i++) {
			os << img[i] << '\n';
		}
		return os;
	}
} // end of namespace s5loo

