#ifndef S5LOO_CIRCLE_HPP
#define S5LOO_CIRCLE_HPP
#include <tuple>
#include "color.hpp"
#include "shape.hpp"
#include "SFML/Graphics.hpp"


namespace s5loo {
	//class Window;
	class Circle : public Shape {
	private:
		double radius_;

	public:
		Circle(double x, 
				double y, 
				double speedX, 
				double speedY, 
				double radius, 
				Color color)
		: Shape{x, y, speedX, speedY, color}, radius_{radius} {}

		double radius() const { return radius_; }
		//void move(const Window &win, double dt);
		void draw(sf::RenderWindow& win) const override;
		double boundingSphere() const override {
			return radius_;
		}
	}; // end of class Circle
} //end of namespace s5loo


#endif