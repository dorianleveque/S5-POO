#ifndef S5LOO_IMAGE_HPP
#define S5LOO_IMAGE_HPP
#include <vector>
#include <string>
#include <stdexcept>
#include <iostream>
#include "color.hpp"

namespace s5loo {
	class Image {
	private:
		std::string name_;
		int width_, height_;
		std::vector<Color> pixels_;

	public:
		Image(std::string n, int w, int h, bool random = true);
		Image(std::string path);
		
		Image(const Image&) = delete; 			//Constructeur par référence
		Image(Image &&) = default;					//Constructeur par déplacement
		Image& operator=(const Image&) = delete;
		Image& operator=(Image &&) = default;
		~Image() = default;

		// Accesseur
		std::string name() const;
		void name(const std::string& name);
		int width() const;
		int height() const;
		Color operator[](int i) const;
		Color& operator[](int i);
	};

	// functions membres
	inline
	std::string Image::name() const { return name_; }

	inline
	void Image::name(const std::string& name) { name_ = name; }

	inline
	int Image::width() const { return width_; }

	inline
	int Image::height() const { return height_; }

	inline
	Color Image::operator[](int i) const {
		if (i <= (width_ * height_)) return pixels_[i];
		else throw std::out_of_range("out of range");
	}

	inline
	Color& Image::operator[](int i) {
		if (i <= (width_ * height_)) return pixels_[i];
		else throw std::out_of_range("out of range");
	}

	// functions non membre
	/* [QUESTION]
		Pourquoi ne pas en avoir fait une fonction membre ?
	*/
	inline
	int size(const Image &img) { return (img.width() * img.height()); }

	std::string imageName(std::string path);

	// surcharge de l'opérateur d'injection <<
	std::ostream& operator<< (std::ostream& os, const Image& img);
}

#endif