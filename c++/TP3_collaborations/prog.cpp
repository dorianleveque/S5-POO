#include <random>
#include "window.hpp"
#include "circle.hpp"

int main(void) {
  s5loo::Window win{"Rectangles and Circles", 800,600};
  auto [winWidth, winHeight] = win.size();

  std::default_random_engine rndGen{std::random_device{}()};
  std::uniform_int_distribution<short> colorDistr{0, 255};
  std::uniform_real_distribution<double> speedDistr{-50, 50};
  std::uniform_real_distribution<double> dimDistr{10, 60};
  std::uniform_real_distribution<double> xposDistr{0, winWidth};
  std::uniform_real_distribution<double> yposDistr{0, winHeight};
  std::uniform_real_distribution<double> angularDistr{-30, 30};


  s5loo::Circle c1 = s5loo::Circle( xposDistr(rndGen),  //x
                                    yposDistr(rndGen),  //y
                                    speedDistr(rndGen), //speed x
                                    speedDistr(rndGen), //speed y
                                    dimDistr(rndGen),   //radius
                                    { 
                                      (unsigned char)colorDistr(rndGen), // red
                                      (unsigned char)colorDistr(rndGen), // green
                                      (unsigned char)colorDistr(rndGen)  // blue
                                    });

  s5loo::Circle c2 = s5loo::Circle( xposDistr(rndGen),
                                    yposDistr(rndGen),
                                    speedDistr(rndGen),
                                    speedDistr(rndGen),
                                    dimDistr(rndGen),
                                    { 
                                      (unsigned char)colorDistr(rndGen),
                                      (unsigned char)colorDistr(rndGen),
                                      (unsigned char)colorDistr(rndGen)
                                    });

  s5loo::Circle c3 = s5loo::Circle( xposDistr(rndGen),
                                    yposDistr(rndGen),
                                    speedDistr(rndGen),
                                    speedDistr(rndGen),
                                    dimDistr(rndGen),
                                    { 
                                      (unsigned char)colorDistr(rndGen),
                                      (unsigned char)colorDistr(rndGen),
                                      (unsigned char)colorDistr(rndGen)
                                    });

  win.addCircle(std::move(c1));
  win.addCircle(std::move(c2));
  win.addCircle(std::move(c3));
  
  s5loo::Rectangle r1 = s5loo::Rectangle(xposDistr(rndGen),
                                         yposDistr(rndGen),
                                         speedDistr(rndGen),
                                         speedDistr(rndGen),
                                         dimDistr(rndGen),
                                         dimDistr(rndGen),
                                         { 
                                          (unsigned char)colorDistr(rndGen),
                                          (unsigned char)colorDistr(rndGen),
                                          (unsigned char)colorDistr(rndGen)
                                         },
                                         angularDistr(rndGen)
                                         );

  s5loo::Rectangle r2 = s5loo::Rectangle(xposDistr(rndGen),
                                         yposDistr(rndGen),
                                         speedDistr(rndGen),
                                         speedDistr(rndGen),
                                         dimDistr(rndGen),
                                         dimDistr(rndGen),
                                         { 
                                          (unsigned char)colorDistr(rndGen),
                                          (unsigned char)colorDistr(rndGen),
                                          (unsigned char)colorDistr(rndGen)
                                         },
                                         angularDistr(rndGen)
                                         );

  s5loo::Rectangle r3 = s5loo::Rectangle(xposDistr(rndGen),
                                         yposDistr(rndGen),
                                         speedDistr(rndGen),
                                         speedDistr(rndGen),
                                         dimDistr(rndGen),
                                         dimDistr(rndGen),
                                         { 
                                          (unsigned char)colorDistr(rndGen),
                                          (unsigned char)colorDistr(rndGen),
                                          (unsigned char)colorDistr(rndGen)
                                         },
                                         angularDistr(rndGen)
                                         );
  
  win.addRectangle(std::move(r1));
  win.addRectangle(std::move(r2));
  win.addRectangle(std::move(r3));
  win.display();
  return 0;
}
