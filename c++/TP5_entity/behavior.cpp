#include "behavior.hpp"
#include "entity.hpp"

namespace s5loo {

	void Behavior::position(Entity& e, double x, double y) {
		e.x_=x; e.y_=y;
	}

	void Behavior::speed(Entity& e, double sx, double sy) {
		e.sx_=sx; e.sy_=sy;
	}

	void Behavior::angle(Entity& e, double an) {
		e.angle(an);
	}

	void Behavior::angularSpeed(Entity& e, double as) {
		e.angularSpeed_ = as;
	}

	void Behavior::color(Entity& e, Color c) {
		e.color_ = c;
	}

}
