#ifndef S5LOO_BEHAVIOR_HPP
#define S5LOO_BEHAVIOR_HPP

#include "color.hpp"

namespace s5loo {

	class Entity;
	class Window;

	class Behavior {

	public:
		Behavior() = default;

		Behavior(const Behavior &) = delete;
		Behavior(Behavior &&) = delete;
		Behavior& operator=(const Behavior &) = delete;
		Behavior& operator=(Behavior &&) = delete;
		virtual ~Behavior() = default;

		virtual void animate([[maybe_unused]] Entity &e,
									[[maybe_unused]] const Window &win,
									[[maybe_unused]] double dt) {}
		
		virtual void click([[maybe_unused]] Entity &e,
								 [[maybe_unused]] const Window &win,
								 [[maybe_unused]] int xMouseClic,
								 [[maybe_unused]] int yMouseClic) {}

	protected:
		void position(Entity& e, double x, double y);
		void speed(Entity& e, double sx, double sy);
		void angle(Entity& e, double an);
		void angularSpeed(Entity& e, double as);
		void color(Entity& e, Color c);
	}; // end of class behavior

} // end of namespace s5loo

#endif