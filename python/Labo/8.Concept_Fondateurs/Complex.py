# -*-coding:utf-8 -*
class Complex:
	"""
		###################################
		##
		##		 Class Complexe
		##
		## auteur: Dorian
		## version: 0.0
		###################################
	
	== Description:
		Classe permettant de créer des objects symbolisant des nombres complexes
	
	== Appel class:
		Complexe(reel_number, imaginaire_numer)
	
	== Mutateur:
		Clone() => (Conplexe Object)
	
	== Accesseurs:
		show() => (string) reel + img i
	
	"""

	def __init__(self, reel, img):
		self.__reel = reel
		self.__img = img
	
	def __del__(self):
		print "Fin d'un complex !"
	
	def show(self):
		return str(self.__reel) + "+" + str(self.__img) + "i"

	def clone(self):
		return Complex(self.__reel, self.__img)



def afficher_complexe(c):
	print c.show()

def compareComplex(c1, c2):
	return c1.show() == c2.show()



print("=== Début du Programme ===")
c1 = Complex(3,4)
c2 = Complex(3,4)
c3 = Complex(5,3)
print(c1)
afficher_complexe(c1)
print ("c1 : " + c1.show())
print("\n === Test Compare Complex : ")
print("> Compare c1, c2 : \n c1 == c2 ? " + str(compareComplex(c1, c2) if "Oui" else "Non"))
print("> Compare c1, c3 : \n c1 == c3 ? " + str(compareComplex(c1, c3) if "Oui" else "Non"))

print("\n === Clone :")
print c1 == c2
c1 = c2
print c1 == c2
c4 = c1.clone()
print(c1 == c4)

print c1.__doc__
print("=== Fin du programme ===")