# -*-coding:Latin-1 -*
###
# Description: Programme qui calcul la moyenne
# en fonction des notes rentrees.
#
# version 0.1
###

class moyenne():	
	def __init__(self, notes = []):
		self.__notes = notes
	

	### Accesseurs
	def nombre_notes(self):
		return len(self.__notes)

	def recherche_note(self, note):
		return self.__notes.index(note)
		
	def notes_min(self):
		return min(self.__notes)

	def notes_max(self):
		return max(self.__notes)

	### Mutateurs 
	def ajout_note(self, note):
		self.__notes.append(note)

	def calcul(self):
		somme = 0
		for i in range(self.nombre_notes()):
			somme += self.__notes[i]
		return somme / self.nombre_notes()


m = moyenne() 	# { instance moyenne }


####
# lecture notes

nb_note = input('Combien de notes ? : ')
for i in range(nb_note):
	note = input('note '+`i+1`+' : ')
	m.ajout_note(note) #notes.append(note)
####


###	
# Afficher les résultats
print("")
print("Moyenne  : "+`m.calcul()`)
print("Note min : "+`m.notes_min()`	+ "\tindex : " + `m.recherche_note(m.notes_min())`)
print("Note max : "+`m.notes_max()`	+ "\tindex : " + `m.recherche_note(m.notes_max())`)
###
