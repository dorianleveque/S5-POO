#include <cmath>
#include "randomSpeed.hpp"
#include "entity.hpp"
#include "window.hpp"

namespace s5loo {

	void RandomSpeed::click(Entity &e, const Window &win, [[maybe_unused]] int xMouseClic, [[maybe_unused]] int yMouseClic) {
		auto [win_width, win_height] = win.size();
		const double windowCenterX = win_width/2;
		const double windowCenterY = win_height/2;


		double newSpeedX = std::abs(speedDistr_(rndGen_));
		double newSpeedY = std::abs(speedDistr_(rndGen_));

		auto [x, y] = e.position();
		if ((x - windowCenterX) > 0) {
			newSpeedX = -newSpeedX;
		}
		if ((y - windowCenterY) > 0) {
			newSpeedY = -newSpeedY;
		}
			
		angularSpeed(e, angularDistr_(rndGen_));
		speed(e, newSpeedX, newSpeedY);
	}

}	// end of namespace s5loo