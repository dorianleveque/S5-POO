# -*-coding:utf-8 -*
class CompteBancaire:
	"""
		###################################
		##
		##		 Class CompteBancaire
		##
		## auteur: Dorian
		## version: 0.0
		###################################
	
	== Description:
		La classe CompteBancaire permet de créer des objects représentant le compte bancaire d'un client dans la banque.
	
	== Appel class:
		CompteBancaire(nom, solde)
		| => Params:
		|  - nom : (string) default = 'Dupont'
		| 	- solde: (int) default = 1000
	
	== Mutateur:
		depot(somme) 
		| Return => none
		| Description:
		|   Permet d'ajouter une certaine somme au solde

		retrait(somme) 
		| Return => none
		| Description:
		|   Permet retirer une certaine somme au solde
	
	== Accesseurs:
		affiche()
		| Description:
		| 	 Affiche dans la console
		
	
	"""
	def __init__(self, nom='Dupont', solde=1000):
		self.__nom = nom
		self.__solde = solde
	
	def __del__(self):
		print("Destruction du compte bancaire")

	def depot(self, somme):
		self.__solde += somme
	
	def retrait(self, somme):
		self.__solde -= somme
	
	def affiche(self):
		print("Le solde du compte bancaire de "+str(self.__nom)+" est de "+str(self.__solde)+" euros.")

compte1 = CompteBancaire('Duchmol', 800)
compte1.depot(350)
compte1.retrait(200)
compte1.affiche()

compte2 = CompteBancaire()
compte2.depot(25)
compte2.affiche()