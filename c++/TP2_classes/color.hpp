#ifndef S5LOO_COLOR_HPP
#define S5LOO_COLOR_HPP
#include <stdexcept>
#include <iostream>

namespace s5loo {

using uchar = unsigned char;

	struct Color
	{
		uchar red;
		uchar green;
		uchar blue;

		// ------------------
		
		Color(uchar r, uchar g, uchar b) : red {r}, green {g}, blue {b} {}
		Color() : Color(0, 0, 0) {}

		Color(const Color &) = default; 				// Constructeur par recopie
		Color(Color &&) = default;						// Constructeur par déplacement
		Color& operator=(const Color &) = default;	// Affectation par recopie
		Color& operator=(Color &&) = default; 		// Affectation par déplacement
		~Color() = default;								// Destructeur
		
		// ------------------
		uchar operator[](int i) const;
		uchar& operator[](int i);

	}; // struct Color

	inline
	uchar Color::operator[](int i) const { // accès en lecture seulement des attributs
		if (i==0) return red;
		else if (i==1) return green;
		else if (i==2) return blue;
		else throw std::out_of_range {"index out of range"};
	}

	inline
	uchar& Color::operator[](int i) { // accès à l'adresse des attributs pour modification
		if (i==0) return red;
		else if (i==1) return green;
		else if (i==2) return blue;
		else throw std::out_of_range {"index out of range"};
	}

	inline
	std::ostream& operator << (std::ostream& os, const Color& c) {
		return os << (int) c[0] << ' '
				  	 << (int) c[1] << ' '
				  	 << (int) c[2];
	}

	inline
	Color grey(Color c) {
		/* Attention !
		L'opération doit être entière puisqu'il est possible
		d'avoir un dépassement d'octet. 
		*/
		uchar g = (uchar) (((int) c[0] + c[1] + c[2]) / 3); 
		return { g, g, g};
	}
} // end namespace s5loo

#endif