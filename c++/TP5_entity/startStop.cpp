#include <cmath>
#include "startStop.hpp"
#include "entity.hpp"
#include "window.hpp"

namespace s5loo {

	void StartStop::click(Entity &e, const Window &win, [[maybe_unused]] int xMouseClic, [[maybe_unused]] int yMouseClic) {
		auto [sx, sy] = e.speed();
		sx = std::abs(sx);
		sy = std::abs(sy);
		
		if (sx > 0 || sy > 0) {
			sx = 0.0;
			sy = 0.0;
		}
		else {
			sx = speedDistr_(rndGen_);
			sy = speedDistr_(rndGen_);
		}

		//angularSpeed(e, angularDistr_(rndGen_));
		speed(e, sx, sy);
	}

}	// end of namespace s5loo