#ifndef S5LOO_SHAPE_HPP
#define S5LOO_SHAPE_HPP
#include <tuple>
#include "color.hpp"
#include "SFML/Graphics.hpp"


namespace s5loo {
	class Window;
	class Shape {
	private:
		double x_, y_;
		double sx_, sy_;
		Color color_;

	protected:
		virtual void do_click(const Window &win);

	public:
		Shape(double x, 
				double y, 
				double speedX, 
				double speedY,
				Color color)
		: x_{x}, y_{y}, sx_{speedX}, sy_{speedY}, color_{color} {}

		Shape(const Shape&) = delete;
		Shape(Shape&&) = delete;
		Shape& operator=(const Shape&) = delete;
		Shape& operator=(Shape&&) = delete;
		virtual ~Shape() = default;


		std::tuple<double, double>
		position() const { return {x_, y_}; }

		std::tuple<double, double>
		speed() const { return {sx_, sy_}; }

		void speed(double sx, double sy) {
			sx_ = sx;
			sy_ = sy;
		}

		Color
		color() const { return color_; }

		virtual void draw (sf::RenderWindow& win) const = 0;
		
		virtual void move(const Window &win, double dt);

		virtual double boundingSphere() const = 0;

		void click(const Window &win, int xMouseClicCrd, int yMouseClicCrd);
	}; // end of class Shape
} //end of namespace s5loo
#endif