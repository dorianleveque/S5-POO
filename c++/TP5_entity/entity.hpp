#ifndef S5LOO_ENTITY_HPP
#define S5LOO_ENTITY_HPP

#include <tuple>
#include <memory>
#include <vector>
#include "SFML/Graphics.hpp"
#include "color.hpp"
#include "shape.hpp"
#include "behavior.hpp"

namespace s5loo {
	class Window;
	
	class Entity {
	friend class Behavior;

	private:
		double x_, y_;
		double sx_, sy_;
		double angularSpeed_;
		double angle_;
		Color color_;
		std::unique_ptr<Shape> shape_;
		std::vector<std::unique_ptr<Behavior>> behaviors_;

		void angle(double a) {
			angle_ = std::move(a);
			if (angle_ > 180) angle_ -= angle_ - 360;
		}

	public:
		Entity(double x = 0.0,
				 double y = 0.0,
				 double sx = 0.0,
				 double sy = 0.0,
				 double as = 0.0,
				 Color c = Color {0,0,0})
				 : x_{x}, y_{y}, sx_{sx}, sy_{sy}, angularSpeed_{as}, angle_{0.0}, color_{c}, shape_{}, behaviors_{} {}

		Entity(const Entity&) = default;
		Entity(Entity&&) = default;
		Entity& operator=(const Entity&) = default;
		Entity& operator=(Entity&&) = default;
		virtual ~Entity() = default;

		
		/*	position()
		*	@return tupple (x, y)
		*/
		std::tuple<double, double> position() const {
			return {x_, y_};
		}

		/* speed() 
		*	@return tupple (sx, sy)
		*/
		std::tuple<double, double> speed() const {
			return {sx_, sy_};
		}

		/* speed(sx, sy) 
		*	@params { sx: "speed x axes", sy: "speed y axes" }
		*/
		void speed(double sx, double sy) {
			sx_ = sx;
			sy_ = sy;
		}

		/* angle() 
		*	@return angle 
		*/
		double angle() const {
			return angle_;
		}

		double angularSpeed() const {
			return angularSpeed_;
		}

		/* color(sx, sy) 
		*	@return color
		*/
		Color color() const { 
			return color_;
		}

		/* draw()
		*	@Description: Draw the differents shape 
		*/ 
		void draw(sf::RenderWindow& win) {
			shape_ -> draw(*this, win);
		}

		void shape(std::unique_ptr<Shape> s) {
			shape_ = std::move(s);
		}

		double boundingSphere() const { 
			if(shape_) return shape_->boundingSphere();
			else return 0.0;
		}

		void addBehavior(std::unique_ptr<Behavior> b) {
			behaviors_.emplace_back(std::move(b));
		}

		void animate(const Window &win, double dt) {
			for (auto &b: behaviors_) {
				b -> animate(*this, win, dt);
			}
		}

		void click(const Window &win, int xMouseClick, int yMouseClick);

	}; // end of class Entity

} // end of namespace Entity

#endif