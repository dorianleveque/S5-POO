#include "circle.hpp"
#include "SFML/Graphics.hpp"
#include "window.hpp"

namespace s5loo {

	void 
	Circle::move(const Window &win, double dt) {
		auto [width, height] = win.size();
		x_ += sx_ * dt;
		y_ += sy_ * dt;
		
		if (x_ < 0) {
			//x_ = 0;
			sx_= -sx_;//0;
		}
		
		if (x_+2*radius_ >= width) {
			x_ = width-1-2*radius_;
			sx_= -sx_;//0;
		}
		
		if (y_ < 0) {
			//y_ = 0; 
			sy_= -sy_;//0;
		}
		
		if (y_+2*radius_ >= height) { 
			y_ = height-1-2*radius_; 
			sy_= -sy_;//0;
		}
	}

	void
	draw (const Circle &circ, sf::RenderWindow& win) {
		sf::CircleShape s{(float)circ.radius()};

		Color c = circ.color();
		s.setFillColor(sf::Color {c[0], c[1], c[2]});

		auto [x, y] = circ.position();
		s.setPosition((float)x, (float)y);
		win.draw(s);
	}
}