#ifndef S5LOO_MOTION_HPP
#define S5LOO_MOTION_HPP

#include "behavior.hpp"

namespace s5loo {

	class Motion : public Behavior {
	
	public:
		Motion() = default;

		void animate(Entity& e, [[maybe_unused]] const Window &win, double dt) override;

	}; // end of class Motion

}	// end of namespace s5loo

#endif