# -*-coding:utf-8 -*
class Livre():
	"""
		###################################
		##
		##		 Class Livre
		##
		## auteur: Dorian
		## version: 0.0
		###################################
	
	== Description:
		La classe Livre permet de créer des objets
		représentant un livre avec les caractéristiques
		suivantes: auteur, titre, prix, annee de paruption
	
	== Appel class:
		Livre()
		Livre(titre)
		Livre(titre, auteur)
		Livre(titre, auteur, prix)
		Livre(titre, auteur, prix, annee)
	
	== Mutateur:
		aucun
	
	== Accesseurs:
		aucun
	
	"""
	
	def __init__(self,
					 titre = undefined
					 auteur = undefined
					 prix = undefined,
					 annee = undefined):
		self.__titre = titre
		self.__auteur = auteur
		self.__prix = prix
		self.__annee = annee

	def __del__(self):
		print "Destruction du livre"

	livre1 = Livre("Faire des exercices pour les nuls !")
	livre2 = Livre("Comment faire un bon cours", "Jacques CHI", 100, 2017)

	def demande_livre(livre_demander, livreobj):
		return livre_demander == livreobj.__titre