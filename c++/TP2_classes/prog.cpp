#include <iostream>
#include <string>
#include "color.hpp"
#include "image.hpp"

void test_color() {
	std::cout << "\n----- "<<__func__<<"() -----\n";
	s5loo::Color red {255, 0, 0};
	s5loo::Color c;

	std::cout << "\nDéclaration des variables:\n";
	std::cout << "red:\t" << red << '\n';
	std::cout << "c:\t" << c << '\n';
	
	// --------------------
	
	c[1] = 165;
	s5loo::Color g = {grey(red)}; 					// /!\ error  < NO ADL >
	const s5loo::Color noir;

	std::cout << "\nAffichage des 3 objets Color:\n";
	std::cout << "red:\t" << red << '\n';
	std::cout << "c:\t" << c << '\n';
	std::cout << "grey:\t" << g << '\n';
	std::cout << "noir:\t" << noir << '\n';
	std::cout << "noir[0]:" << (int)noir[0] << '\n';		// /!\ error  < NO ACCESS >
}	

void test_simple_image() {
	std::cout << "\n----- "<<__func__<<"() -----\n";
	s5loo::Image img1 {"random",5,3};
	s5loo::Image img2 {"black",3,4,false};
	// -------------------
	std::cout << img1 << '\n';
	std::cout << img2 << '\n';
	//std::cout << img2[50] << '\n';		// test out of range
	//--------------------
	img1.name("undefined");
	std::cout << "Changement de nom img1:\n"
				 << img1 << '\n';
}

void test_image(std::string path) {
	s5loo::Image img;
	if (path != "") {
		img = s5loo::Image(path);
	}
	else {
		std::string name;
		int width, height;
		std::cout << "Nom de l'image: ";
		std::cin >> name;
		std::cout << "largeur: ";
		std::cin >> width;
		std::cout << "hauteur: ";
		std::cin >> height;
		img = s5loo::Image(name, width, height);
	}

}


int main(int argc, char **argv) {
	std::cout << "\n----- "<<__func__<<"() -----\n";
	//std::cout << "argc: " << argc << " argv[1]: " << argv[1] << '\n';
	
	if (argc>1) {
		std::string arg_str = argv[1];
		if (arg_str == "color") 			test_color();
		if	(arg_str == "simple_image") 	test_simple_image();	
		if (arg_str == "image") {
			if (argc < 3) test_image("");
			else test_image((std::string) argv[2]);
		}					
	}
	else {
		std::cout << "\nFor run this application, use these commands:\n"
					 << "./prog color\n"
					 << "./prog simple_image\n";
	}
	return 0;
}