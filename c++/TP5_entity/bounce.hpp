#ifndef S5LOO_BOUNCE_HPP
#define S5LOO_BOUNCE_HPP

#include "behavior.hpp"

namespace s5loo {

	class Bounce: public Behavior {

	public:
		Bounce() = default;

		void animate(Entity& e, const Window &win, double dt) override;

	}; // end of class Behavior

}	// end of namespace s5loo

#endif