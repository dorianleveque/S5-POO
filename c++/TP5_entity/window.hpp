#ifndef S5LOO_WINDOW_HPP
#define S5LOO_WINDOW_HPP 1

#include <vector>
#include <tuple>
#include "SFML/Graphics.hpp"
#include "entity.hpp"

namespace s5loo {

  class Window {
  
  private :
    std::string name_;
    double width_, height_;
    sf::RenderWindow win_;
    std::vector<Entity> entities_;

  public :
    Window(std::string n="Window", double w=700, double h=500);
    
    Window(const Window&) = delete;
    Window(Window&&) = default;
    Window& operator=(const Window&) = delete;
    Window& operator=(Window&&) = default;
    virtual ~Window() = default;

    std::tuple<double,double> size() const;
    void display(void);
    Entity& addEntity(Entity e);
    void drawAll();
    void moveAll(double dt);
    void clickAll(int xMouseClicCrd, int yMouseClicCrd);

  };  // class Window

  /* inline member functions */

  inline
  std::tuple<double,double> Window::size() const { return {width_, height_}; }

  inline
  Entity& Window::addEntity(Entity e) {
    return entities_.emplace_back(std::move(e));
  }

  inline
  void Window::drawAll() {
    for (auto &entitie: entities_) {
      entitie.draw(win_);
    }
  }

  inline
  void Window::moveAll(double dt) {
    for (auto &entitie: entities_) {
      entitie.animate(*this, dt);
    }
  }

  inline
  void Window::clickAll(int xMouseClicCrd, int yMouseClicCrd) {
    for (auto &entitie: entities_) {
      entitie.click(*this, xMouseClicCrd, yMouseClicCrd);
    }
  }

  /* global functions déclaration */

  double // seconds since 1970/01/01 00:00:00 UTC (avec des décimales allant jusqu'aux microsecondes)
  getTime();


}  // namespace s5loo

#endif
