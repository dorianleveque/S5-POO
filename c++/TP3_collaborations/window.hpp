#ifndef S5LOO_WINDOW_HPP
#define S5LOO_WINDOW_HPP 1

#include <vector>
#include <tuple>
#include "SFML/Graphics.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

namespace s5loo {

  class Window {
  private :
    std::string name_;
    double width_, height_;
    sf::RenderWindow win_;
    std::vector<Circle> circles_;
    std::vector<Rectangle> rectangle_;


  public :
    Window(std::string n="Window", double w=700, double h=500);
      
    /* Special functions -- copy constructor and copy assignement are deleted */
    Window(const Window&) = delete;
    Window(Window&&) = default;
    Window& operator=(const Window&) = delete;
    Window& operator=(Window&&) = default;
    virtual ~Window() = default;

    std::tuple<double,double> size() const;
    void display(void);
    void addCircle(Circle c);
    void addRectangle(Rectangle r);
    void drawAll();
    void moveAll(double dt);

  };  // class Window

  /* inline member functions */

  inline
  std::tuple<double,double> Window::size() const { return {width_, height_}; }

  inline
  void Window::addCircle(Circle c) {
    circles_.emplace_back(std::move(c));
  }

  inline
  void Window::addRectangle(Rectangle r) {
    rectangle_.emplace_back(std::move(r));
  }

  inline
  void Window::drawAll() {
    for (const auto &circle: circles_) {
      draw(circle, win_);
    }

    for (const auto &rectangle: rectangle_) {
      draw(rectangle, win_);
    }
  }

  inline
  void Window::moveAll(double dt) {
    for (auto &circle: circles_) {
      circle.move(*this, dt);
    }

    for (auto &rectangle: rectangle_) {
      rectangle.move(*this, dt);
    }
  }


  /* global functions déclaration */

  double // seconds since 1970/01/01 00:00:00 UTC (avec des décimales allant jusqu'aux microsecondes)
  getTime();


}  // namespace s5loo

#endif
