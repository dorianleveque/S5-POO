#ifndef MODULE_VECTOR_HPP
#define MODULE_VECTOR_HPP

#include <vector>
#include <tuple>

namespace s5loo {
	using intvec = std::vector<int>;

	intvec makeVector(int count, int nMin=10, int nMax=100);

	void printVector(const intvec v);
	int numEven(intvec v);
	std::tuple<double, double, double>dataVector(intvec v);

}
#endif