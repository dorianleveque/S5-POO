# -*-coding:utf-8 -*
class Voiture:
	"""
		###################################
		##
		##		 Class Voiture
		##
		## auteur: Dorian
		## version: 0.0
		###################################
	
	== Description:
		Create Voiture instance that reproduct the car comportement
	
	== Appel class:
		Voiture(marque, couleur, pilote, vitesse)
		| Params:
		|   - marque: (string)  | Marque de la voiture        | default = "Ford"
		| 	 - couleur: (string) | Couleur de la voiture       | default = "rouge"
		|   - pilote: (string)  | Nom du pilote de la voiture | default = "personne"
		|   - vitesse: (number) | Vitesse de la voiture       | default = 0 
	
	== Mutateur:
		choix_conducteur((string) nom)
		| Return => none
		| Description:
		|   Permettra de désigner (ou de changer) le nom du conducteur

		accelerer((number)taux, (number)duree)
		| Return => none
		| Description:
		|   Permettra de faire varier la vitesse de la voiture
	
	== Accesseurs:
		affiche_tout()
		| Return => none
		| Description:
		|   Permettra de faire apparaitre dans la console les propriétés présentes de la voiture
	
	"""
	def __init__(self,
					 marque = "Ford",
					 couleur = "rouge",
					 pilote = "personne",
					 vitesse = 0):
		self.__marque = marque
		self.__couleur = couleur
		self.__pilote = pilote
		self.__vitesse = vitesse
	
	def __del__(self):
		print("Destruction de la voiture")

	def choix_conducteur(self, pilote):
		self.__pilote = pilote

	def accelerer(self, taux, duree):
		if self.__pilote != "personne":
			self.vitesse = taux * duree
		else:
			print "Cette voiture n'a pas de conducteur !"

	def affiche_tout(self):
		print str(self.__marque) + " " + str(self.__couleur) + " pilote par " + str(self.__pilote) + ", vitesse = " + str(self.__vitesse) + " m/s."


v1 = Voiture('Peugeot', 'bleue')
v2 = Voiture(couleur = 'verte')
v3 = Voiture('Mercedes')
v1.choix_conducteur('Romeo')
v2.choix_conducteur('Juliette')
v2.accelerer(1.8, 12)
v3.accelerer(1.9, 11)
v2.affiche_tout()
v3.affiche_tout()
print v1.__doc__