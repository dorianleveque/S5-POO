#ifndef S5LOO_THROUGH_HPP
#define S5LOO_THROUGH_HPP

#include "behavior.hpp"

namespace s5loo {

	class Through: public Behavior {

	public:
		Through() = default;

		void animate(Entity& e, const Window &win, double dt) override;

	}; // end of class Through

}	// end of namespace s5loo

#endif