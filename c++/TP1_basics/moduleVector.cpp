#include <random>
#include <iostream>
#include <stdexcept>
#include <cmath>
#include <tuple>
#include <algorithm>
#include "moduleVector.hpp"

namespace s5loo {
	
	intvec makeVector(int count, int nMin, int nMax) {
		std::default_random_engine rndGen	{std::random_device{}()};
		std::uniform_int_distribution<int> number	{nMin, nMax};

		intvec list;
		for (int i=0; i<count; i++) {
			list.emplace_back(number(rndGen));
		}
		return list;
	}


	void printVector(const intvec &v) {
		std::cout << "{ ";
		for (const auto &s: v) {
			std::cout << s << ' ';
		}
		std::cout << "}\n";
	}


	int numEven(const intvec &v) {
		int nbPair = 0;
		for (const auto &s: v) {
			if (s%2 == 0) nbPair++;
		}
		return nbPair;
	}


	std::tuple<double,
				  double,
				  double>
				  dataVector(const intvec &v) {
		if (v.size() == 0) throw std::runtime_error {"vector empty !"};


		double average = 0.0;
		for (const auto &s: v) average += s;
		average /= (double) v.size();


		double ecartType = 0.0;
		for (const auto &s: v) {
			ecartType += (s - average)*(s - average);
		}
		ecartType = std::sqrt(ecartType/(double)v.size());


		double median = 0.0;
		intvec copyVec {v};
		std::sort(copyVec.begin(), copyVec.end());
		if (copyVec.size()%2) {
			//impair
			median = copyVec[int(copyVec.size()/2)];
		}		
		else {
			// pair
			median = (copyVec[int(copyVec.size()/2)] + copyVec[int(copyVec.size()/2)-1])/2;
		}


		return {std::move(average), 
				  std::move(ecartType), 
				  std::move(median)};
	}
}