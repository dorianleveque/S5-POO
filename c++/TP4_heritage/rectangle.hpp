#ifndef S5POO_RECTANGLE_HPP
#define S5POO_RECTANGLE_HPP
#include <tuple>
#include <cmath>
#include "color.hpp"
#include "shape.hpp"
#include "SFML/Graphics.hpp"

namespace s5loo {
	//class Window;
	class Rectangle : public Shape {
	private:
		double width_, height_;
		double angularSpeed_;
		double angle_;

		void angle(double a) {
			angle_ = std::move(a);
			if (angle_ > 180) angle_ -= 360 - angle_;
		}

	public:
		Rectangle(double x,
					 double y,
					 double sx,
					 double sy,
					 double width,
					 double height,
					 Color color,
					 double angularSpeed = 0.0)
					 : Shape{x, y, sx, sy, color}, width_{width}, height_{height}, angularSpeed_{angularSpeed},angle_ {0.0} {}

		std::tuple<double, double> size() const { return {width_, height_}; }
		double angle() const { return angle_; }
		void move(const Window &win, double dt) override;
		void draw(sf::RenderWindow& win) const override;
		double boundingSphere() const override {
			return std::sqrt(width_*width_ + height_*height_) / 2;
		}
		void do_click(const Window &win) override;
	}; //end of class Rectangle
} // end of namespace s5loo
#endif