# -*-coding:utf-8 -*
##############################################################
##		Sujet : GAIA														##
##																				##
## 	Description:														##
##		Ce sujet porte sur la modéisation et la simulation		##
##		de la diffusion de chaleur à la surface de la			##
##		planète																##
##																				##
## 	auteur: Dorian L													##
## 	date: 26-02-18														##
##############################################################

class Cellule:
	"""
		###################################
		##
		##		 Class Cellule
		##
		###################################
	
	== Description:
		Chacune d'elle possède des caractèristiques physiques
		(température, coefficient de diffusion, coefficient de
		perte). La température d'une cellule va conditionner sa
		couleur.
	
	"""
	def __init__(self, t, x, y, temp, coef_diff, coef_perte, couleur=125):
		self.__terrain = t
		self.__temperature = temp
		self.__coef_diffusion = coef_diff
		self.__x = x
		self.__y = y
		self.__coef_perte = coef_perte
		self.__couleur = couleur

	def __del__(self):
		print "Destruction de la cellule (" + str(x) +", "+ str(y) +")"

	def get_position(self):
		return (self.__x, self.__y)

	def update(self):
		tmin = self.__terrain.get_tmin()
		
		# temperatures des cellules
		Tx_dessus = sum((sum(self.__terrain.celluleDessus()) - self.__temperature) / len(self.__terrain.celluleDessus())
		
		
		Tx_dessous = sum((sum(self.__terrain.celluleDessous()) - self.__temperature) / len(self.__terrain.celluleDessous())
		Tx_gauche = sum(self.__terrain.celluleDessus())
		Tx_droite = sum(self.__terrain.celluleDessus())

		Tx = max()
		self.__temperature = max()










class Soleil:
	"""
		###################################
		##
		##		 Class Soleil
		##
		## auteur: Dorian
		## version: 0.0
		###################################
	
	== Description:
		Le soleil est un élément particulier qui est associé à une
		cellule qu'il chauffe
	
	== Appel class:
		Soleil()
	
	== Mutateur:
		
	
	== Accesseurs:
		
	
	"""






class Terrain:
	"""
		###################################
		##
		##		 Class Terrain
		##
		###################################
	
	== Description:
		Le terrain est rectangulaire et set composé de cellules
	"""
	def __init__(self, x, y, tempMoy = 16, TMIN = 0):
		self.__temperatureMoyenne = tempMoy
		self.__TMIN = TMIN²
		self.__nbCelluleX = x
		self.__nbCelluleY = y
		self.__cellules = [] // tableau de cellule (x, y)

	def update(self):
		for l in self.__cellules:
			for c in l:
				// soleil.update()
				c.update()

	def get_tempMoy(self):
		return self.__temperatureMoyenne
	
	def get_tmin(self):
		return self.__TMIN
	
	def celluleDessus(cel):
		list_cel = []
		pos_cel = cel.get_position() # position de la cellule sur le terrain
		for i in self.__cellules:
			if i.get_position()[1] == pos_cel[1]:	# meme col
				if i.get_position()[0] < pos_cel[0]	# < ligne
					list_cel.append(i)
		return list_cel

	def celluleDessous(cel):
		list_cel = []
		pos_cel = cel.get_position() # position de la cellule sur le terrain
		for i in self.__cellules:
			if i.get_position()[1] == pos_cel[1]:	# meme col
				if i.get_position()[0] > pos_cel[0]	# > ligne
					list_cel.append(i)
		return list_cel

	def celluleGauche(cel):
		list_cel = []
		pos_cel = cel.get_position() # position de la cellule sur le terrain
		for i in self.__cellules:
			if i.get_position()[0] == pos_cel[0]:	# meme ligne
				if i.get_position()[1] < pos_cel[1]	# < col
					list_cel.append(i)
		return list_cel

	def celluleDroite(cel):
		list_cel = []
		pos_cel = cel.get_position() # position de la cellule sur le terrain
		for i in self.__cellules:
			if i.get_position()[0] == pos_cel[0]:	# meme ligne
				if i.get_position()[1] > pos_cel[1]	# > col
					list_cel.append(i)
		return list_cel



class Simulateur:
	"""
		###################################
		##
		##		 Class Simulateur
		##
		## auteur: Dorian
		## version: 0.0
		###################################
	
	== Description:
		
	
	== Appel class:
		
	
	== Mutateur:
		
	
	== Accesseurs:
		
	
	"""
