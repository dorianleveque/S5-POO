#include "circle.hpp"
#include "SFML/Graphics.hpp"

namespace s5loo {

	void
	Circle::draw(Entity &e, sf::RenderWindow& win) const {
		double r = radius();
		sf::CircleShape s{(float)r};

		Color c = e.color();
		s.setFillColor(sf::Color {c[0], c[1], c[2]});

		auto [x, y] = e.position();
		s.setOrigin((float)r, (float)r);
		s.setPosition((float)x, (float)y);
		win.draw(s);
	}

} // end of namespace s5loo