#include <random>
#include "rectangle.hpp"

namespace s5loo {

	void Rectangle::draw(Entity &e, sf::RenderWindow& win) const {
		auto [x, y] = e.position();
		auto [rectWidth, rectHeight] = size();
		Color rectColor = e.color();
		double a = e.angle();
		
		sf::RectangleShape s{sf::Vector2f{(float)rectWidth, (float)rectHeight}};
		s.setFillColor(sf::Color{rectColor[0], rectColor[1], rectColor[2]});
		s.setOrigin((float)(rectWidth*0.5), (float)(rectHeight*0.5));
		s.setPosition((float)x, (float)y);
		s.setRotation((float)a);		
		win.draw(s);
	}

} //end of namespace s5loo