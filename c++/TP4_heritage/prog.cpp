#include <random>
#include <memory>
#include "window.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

using namespace s5loo;

int main(void) {
  Window win{"Rectangles and Circles", 800,600};
  auto [winWidth, winHeight] = win.size();

  std::default_random_engine rndGen{std::random_device{}()};
  std::uniform_int_distribution<short> colorDistr{0, 255};
  std::uniform_real_distribution<double> speedDistr{-50, 50};
  std::uniform_real_distribution<double> dimDistr{10, 60};
  std::uniform_real_distribution<double> xposDistr{0, winWidth};
  std::uniform_real_distribution<double> yposDistr{0, winHeight};
  std::uniform_real_distribution<double> angularDistr{-30, 30};

  win.addShape(std::make_unique<Circle>(xposDistr(rndGen),  //x
                                        yposDistr(rndGen),  //y
                                        speedDistr(rndGen), //speed x
                                        speedDistr(rndGen), //speed y
                                        dimDistr(rndGen),   //radius
                                        Color{ 
                                          (unsigned char)colorDistr(rndGen), // red
                                          (unsigned char)colorDistr(rndGen), // green
                                          (unsigned char)colorDistr(rndGen)  // blue
                                        }));

  win.addShape(std::make_unique<Circle>(xposDistr(rndGen),  //x
                                        yposDistr(rndGen),  //y
                                        speedDistr(rndGen), //speed x
                                        speedDistr(rndGen), //speed y
                                        dimDistr(rndGen),   //radius
                                        Color{ 
                                          (unsigned char)colorDistr(rndGen), // red
                                          (unsigned char)colorDistr(rndGen), // green
                                          (unsigned char)colorDistr(rndGen)  // blue
                                        }));

  win.addShape(std::make_unique<Circle>(xposDistr(rndGen),  //x
                                        yposDistr(rndGen),  //y
                                        speedDistr(rndGen), //speed x
                                        speedDistr(rndGen), //speed y
                                        dimDistr(rndGen),   //radius
                                        Color{ 
                                          (unsigned char)colorDistr(rndGen), // red
                                          (unsigned char)colorDistr(rndGen), // green
                                          (unsigned char)colorDistr(rndGen)  // blue
                                        }));
  
  win.addShape(std::make_unique<s5loo::Rectangle>(xposDistr(rndGen),
                                                  yposDistr(rndGen),
                                                  speedDistr(rndGen),
                                                  speedDistr(rndGen),
                                                  dimDistr(rndGen),
                                                  dimDistr(rndGen),
                                                  Color{ 
                                                    (unsigned char)colorDistr(rndGen),
                                                    (unsigned char)colorDistr(rndGen),
                                                    (unsigned char)colorDistr(rndGen)
                                                  },
                                                  angularDistr(rndGen)
                                                  ));

  win.addShape(std::make_unique<s5loo::Rectangle>(xposDistr(rndGen),
                                                  yposDistr(rndGen),
                                                  speedDistr(rndGen),
                                                  speedDistr(rndGen),
                                                  dimDistr(rndGen),
                                                  dimDistr(rndGen),
                                                  Color{ 
                                                    (unsigned char)colorDistr(rndGen),
                                                    (unsigned char)colorDistr(rndGen),
                                                    (unsigned char)colorDistr(rndGen)
                                                  },
                                                  angularDistr(rndGen)
                                                  ));
  
  win.addShape(std::make_unique<s5loo::Rectangle>(xposDistr(rndGen),
                                                  yposDistr(rndGen),
                                                  speedDistr(rndGen),
                                                  speedDistr(rndGen),
                                                  dimDistr(rndGen),
                                                  dimDistr(rndGen),
                                                  Color{ 
                                                    (unsigned char)colorDistr(rndGen),
                                                    (unsigned char)colorDistr(rndGen),
                                                    (unsigned char)colorDistr(rndGen)
                                                  },
                                                  angularDistr(rndGen)
                                                  ));

  win.display();
  return 0;
}
