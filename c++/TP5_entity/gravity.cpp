#include "gravity.hpp"
#include "entity.hpp"
#include "window.hpp"

namespace s5loo {

	void
	Gravity::animate(Entity& e, [[maybe_unused]] const Window &win, double dt) {
		auto [x, y] = e.position();
		auto [sx, sy] = e.speed();

		//sx += a * dt;
		sy += (masse_(rndGen_) * constGravitationnelle_) * dt;

		x += sx * dt;
		y += sy * dt;

		angle(e, e.angle() + e.angularSpeed() * dt);
		position(e, x, y);
		speed(e, sx, sy);
	}
}