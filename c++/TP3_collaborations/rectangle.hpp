#ifndef S5POO_RECTANGLE_HPP
#define S5POO_RECTANGLE_HPP
#include <tuple>
#include "color.hpp"
#include "SFML/Graphics.hpp"

namespace s5loo {
	class Window;
	class Rectangle {
	private:
		double x_, y_;
		double sx_, sy_;
		double width_, height_;
		Color color_;
		double angularSpeed_;
		double angle_;

		void angle(double a) {
			angle_ = std::move(a);
			if (angle_ > 180) angle_ -= angle_ - 360;
		}

	public:
		Rectangle(double x,
					 double y,
					 double sx,
					 double sy,
					 double width,
					 double height,
					 Color color,
					 double angularSpeed = 0.0)
					 : x_{x}, y_{y}, sx_{sx}, sy_{sy}, width_{width}, height_{height}, color_{color}, angularSpeed_{angularSpeed}, angle_ {0.0} {}

		Rectangle(const Rectangle&) = default;
		Rectangle(Rectangle&&) = default;
		Rectangle& operator=(const Rectangle&) = default;
		Rectangle& operator=(Rectangle&&) = default;
		~Rectangle() = default;

		std::tuple<double, double> position() 	const { return {x_, y_}; }
		std::tuple<double, double> speed() 		const { return {sx_, sy_}; }
		std::tuple<double, double> size() 		const { return {width_, height_}; }
		Color color() const { return color_; }
		double angle() const { return angle_; }
		void move(const Window &win, double dt);


	}; //end of class Rectangle

	void draw(const Rectangle& rect, sf::RenderWindow& win);

} // end of namespace s5loo
#endif