#ifndef S5LOO_SHAPE_HPP
#define S5LOO_SHAPE_HPP
#include <tuple>
#include "SFML/Graphics.hpp"

namespace s5loo {
	class Entity;
	
	class Shape {

	public:
		Shape() = default;

		Shape(const Shape&) = delete;
		Shape(Shape&&) = delete;
		Shape& operator=(const Shape&) = delete;
		Shape& operator=(Shape&&) = delete;
		virtual ~Shape() = default;

		virtual void draw (Entity &entity, sf::RenderWindow& win) const = 0;

		virtual double boundingSphere() const = 0;

	}; // end of class Shape
} //end of namespace s5loo
#endif