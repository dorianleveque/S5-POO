#ifndef S5LOO_GRAVITY_HPP
#define S5LOO_GRAVITY_HPP

#include <random>
#include "behavior.hpp"

namespace s5loo {

	class Gravity : public Behavior {
	
	private:
		double constGravitationnelle_ = 9.81;
		std::uniform_real_distribution<double> masse_ {0, 50};
		std::default_random_engine rndGen_{std::random_device{}()};

	public:
		Gravity() = default;

		void animate(Entity& e, [[maybe_unused]] const Window &win, double dt) override;

	}; // end of class Gravity

}	// end of namespace s5loo

#endif