class Parisien:
	def __init__(self, nom='undefined', prenom='undefined', age=-1):
		self.__nom = nom
		self.__prenom = prenom
		self.__age = age

	def __del__(self):
		print "Destruction d'un Parisien"

	def prendreMetro(self):
		print str(self.__prenom) +" "+str(self.__nom)+" prend le Métro."

	def dormir(self):
		print str(self.__prenom) +" "+str(self.__nom)+" dort."

class Boulanger(Parisien):
	def __init__(self):
		Parisien.__init__(self)

	def __del__(self):
		