# -*-coding:utf-8 -*
class Domino:
	"""
		###################################
		##
		##		 Class Domino
		##
		## auteur: Dorian
		## version: 0.0
		###################################

	== Description:
		Classe Domino permet de créer des objets domino afin de simuler les pièces d'un jeu de dominos.

	== Appel class:
		Domino(a, b)
			- a : (int) face A
			- b : (int) face B

	== Mutateur:
		

	== Accesseurs:
		affiche_points() => Affiche dans la console les points présents sur les deux faces
		valeur() => (int) Somme des points présent sur les deux faces

	"""
	def __init__(self, faceA = 0, faceB = 0):
		self.__faceA = faceA
		self.__faceB = faceB
	
	def __del__(self):
		print "Fin du dé"

	def affiche_points(self):
		print "face A : "+str(self.__faceA)+" face B : "+str(self.__faceB)

	def valeur(self):
		return self.__faceA+self.__faceB

d1 = Domino(2,6)
d2 = Domino(4,3)
d1.affiche_points()
d2.affiche_points()

print(d1.__doc__)

liste_dominos = []

for i in range(7):
	liste_dominos.append(Domino(6,i))

for i in liste_dominos:
	i.affiche_points()