# -*-coding:utf-8 -*
class Point:
	"""
		###################################
		##
		##		 Class Point
		##
		## auteur: Dorian
		## version: 0.1
		###################################
	
	== Description:
		La classe point permet de representer à l'aide de coordonnée (x, y)
		un point dans un espace de dimension 2
	
	== Appel class:
		Point() // default x=0, y=0
		Point(x, y)
	
	== Mutateur:
		
	
	== Accesseurs:
		
	
	"""
	def __init__(self, x=0, y=0):
		self.__x = x
		self.__y = y

	def __del__(self):
		print "Destruction du point ("+str(self.__x)+", "+str(self.__y)+")"

	def getPoint(self, a=None):
		if a != None:
			if a == 'x':
				return self.__x
			elif a == 'y':
				return self.__y
			else:
				Exception("Vous devez rentrer soit 'x' ou 'y'")
		else:
			return (self.__x, self.__y)
			

	def modifyCrd(self, t): # { 'x': , 'y': }
		self.__x = (t.x) if t.x else self.__x
		self.__y = (t.y) if t.y else self.__y

#####################################################

class Rectangle:
	"""
		###################################
		##
		##		 Class Rectangle
		##
		## auteur: Dorian
		## version: 0.0
		###################################

	== Description:
		La classe Rectangle permet de représenter à l'aide de point, d'un attribut largeur et longueur un rectangle dans un repère de dimension 2. 

	== Appel class:
		Rectangle(Point, longueur, largeur)
		| @Params: 
		|   - Point (Object): position du coin supérieur gauche
		|   - longueur (number) [0]: longueur du rectangle
		|   - largeur (number) [0]: largeur du rectangle
		| @Return: null

	== Mutateur:
		

	== Accesseurs:
		

	"""

	def __init__(self, point, largeur=0, hauteur=0):
		if (isinstance(point, Point)):
			self.__p = point
		else:
			Exception("Un object Point doit être passé en paramètre")
		self.__largeur = largeur
		self.__hauteur = hauteur
	
	def __del__(self):
		print "Destruction du rectangle de coordonnée "+str(self.getInitPoint())+", de largeur "+str(self.__largeur)+" et de hauteur "+str(self.__hauteur)
		del self.__p

	def getInitPoint(self, a=None):
		return self.__p.getPoint(a)

	def setLargeur(self, l):
		self.__largeur = l
	def getLargeur(self):
		return self.__largeur

	def setHauteur(self, h):
		self.__hauteur = h
	def getHauteur(self):
		return self.__hauteur

	def trouveCentre(self):
		initPoint_x = self.getInitPoint('x')
		initPoint_y = self.getInitPoint('y')
		x_center = (initPoint_x + self.getLargeur())/2
		y_center = (initPoint_y + self.getLargeur())/2
		return Point(x_center, y_center)

rectangle1 = Rectangle(Point(12,27), 50, 35)
print rectangle1.trouveCentre()
rectangle1.setHauteur(rectangle1.getHauteur()+20)
rectangle1.setLargeur(rectangle1.getLargeur()-5)
