#ifndef S5POO_RECTANGLE_HPP
#define S5POO_RECTANGLE_HPP
#include <tuple>
#include <cmath>
#include "SFML/Graphics.hpp"
#include "shape.hpp"
#include "entity.hpp"

namespace s5loo {

	class Rectangle : public Shape {
	private:
		double width_, height_;

	public:
		Rectangle(double width, double height)
		: width_{width}, height_{height} {}

		std::tuple<double, double> size() const {
			return {width_, height_};
		}

		void draw(Entity &e, sf::RenderWindow& win) const override;
		
		double boundingSphere() const override {
			return std::sqrt(width_*width_ + height_*height_) / 2;
		}
		
	}; //end of class Rectangle
} // end of namespace s5loo
#endif