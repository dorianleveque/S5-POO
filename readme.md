# S5 POO (Programmation orienté objet)

## Description: 
>Répertoire du cours de POO du semestre printemps 2018 où ce trouve mon code travaillé en cours à l'ENIB. 
Le but du cours ce semestre est de présenter deux langages de programmation orientés objet : C++ et Python comme possibles implémentation des concepts objets étudiés à l'aide du  langage UML. <br>
Il permet notamment de jouer avec la bibliothèque SFML en C++

![alt text](c++/TP.png)

## Présentation de l'archive
Dans l'archive, on trouve un fichier contentant le code Python et un autre le code C++.

## Info pour le C++
>Installation de l'environnement de travail:
```bash
sudo add-apt-repository ppa:ubuntu-toolchain-r/test ↵
sudo apt-get update ↵
sudo apt-get install make gdb g++-8 ↵
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-8 30 ↵
sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-8 30 ↵
```


>Installation des dépendances utilisé dans l'archive
```bash
sudo apt-get install libsfml-dev
```

>Le code C++ fournit dans l'archive n'est pas compilé, pour le compilé il suffit de suivre les étapes suivantes:

1. Ouvrir un terminal à la racine du projet
2. Tapez dans le terminal la commande:

    ```bash
    make
    ```
3. Une fois le fichier compilé, executer le fichier avec la commande

    ```bash
    ./prog
    ```
4. Si une erreur survient à propos d'une bibliothèque (comme ce fut le cas pour moi sur Windows avec le sous-systeme Linux), tapez dans le terminal la commande

    ```bash
    export DISPLAY=:0
    ```
	puis re-essayez d'executer le fichier "prog".

### Bonus

Sur Windows avec le sous-systeme Linux embarqué sur Windows 10, pour installer la biliothèque graphique, il suffit de l'installer sur Linux puis d'installer le logiciel "Xming" sur Windows qui permet d'afficher le rendu graphique qu'exporte Linux à l'aide de la commande "export".