#include <random>
#include <memory>
#include "window.hpp"
#include "circle.hpp"
#include "color.hpp"
#include "rectangle.hpp"
#include "entity.hpp"
#include "motion.hpp"
#include "randomSpeed.hpp"
#include "bounce.hpp"
#include "through.hpp"
#include "startStop.hpp"
#include "gravity.hpp"

using namespace s5loo;

int main(void) {
  Window win{"Rectangles and Circles", 800,600};
  auto [winWidth, winHeight] = win.size();

  std::default_random_engine rndGen{std::random_device{}()};
  std::uniform_int_distribution<short> colorDistr{0, 255};
  std::uniform_real_distribution<double> speedDistr{-50, 50};
  std::uniform_real_distribution<double> dimDistr{10, 60};
  std::uniform_real_distribution<double> xposDistr{0, winWidth};
  std::uniform_real_distribution<double> yposDistr{0, winHeight};
  std::uniform_real_distribution<double> angularDistr{-30, 30};

  const int nbCircle = 8;
  const int nbRectangle = 8;

  for (int i=0; i<nbCircle; i++) {
    Entity& e = win.addEntity(Entity {xposDistr(rndGen),
                                      yposDistr(rndGen),
                                      speedDistr(rndGen),
                                      speedDistr(rndGen),
                                      angularDistr(rndGen),
                                      Color {
                                        (unsigned char) colorDistr(rndGen),
                                        (unsigned char) colorDistr(rndGen),
                                        (unsigned char) colorDistr(rndGen)
                                      }                                
                                      });
    e.shape(std::make_unique<Circle>(dimDistr(rndGen)));
    
    if(i==0) {
      e.addBehavior(std::make_unique<Motion>());
      e.addBehavior(std::make_unique<Bounce>());
      e.addBehavior(std::make_unique<RandomSpeed>());
    }
    if(i==1) {
      e.addBehavior(std::make_unique<Motion>());
      e.addBehavior(std::make_unique<Bounce>());
      e.addBehavior(std::make_unique<StartStop>());
    }
    if(i==2) {
      e.addBehavior(std::make_unique<Motion>());
      e.addBehavior(std::make_unique<Through>());
      e.addBehavior(std::make_unique<RandomSpeed>());
    }
    if(i==3) {
      e.addBehavior(std::make_unique<Motion>());
      e.addBehavior(std::make_unique<Through>());
      e.addBehavior(std::make_unique<StartStop>());
    }
    if(i==4) {
      e.addBehavior(std::make_unique<Gravity>());
      e.addBehavior(std::make_unique<Bounce>());
      e.addBehavior(std::make_unique<RandomSpeed>());
    }
    if(i==5) {
      e.addBehavior(std::make_unique<Gravity>());
      e.addBehavior(std::make_unique<Bounce>());
      e.addBehavior(std::make_unique<StartStop>());
    }
    if(i==6) {
      e.addBehavior(std::make_unique<Gravity>());
      e.addBehavior(std::make_unique<Through>());
      e.addBehavior(std::make_unique<RandomSpeed>());
    }
    if(i==7) {
      e.addBehavior(std::make_unique<Gravity>());
      e.addBehavior(std::make_unique<Through>());
      e.addBehavior(std::make_unique<StartStop>());
    }
  }

for (int i=0; i<nbRectangle; i++) {
    Entity& e = win.addEntity(Entity {xposDistr(rndGen),
                                      yposDistr(rndGen),
                                      speedDistr(rndGen),
                                      speedDistr(rndGen),
                                      angularDistr(rndGen),
                                      Color {
                                        (unsigned char) colorDistr(rndGen),
                                        (unsigned char) colorDistr(rndGen),
                                        (unsigned char) colorDistr(rndGen)
                                      }                                
                                      });
    e.shape(std::make_unique<Rectangle>(dimDistr(rndGen), dimDistr(rndGen)));
    
    if(i==0) {
      e.addBehavior(std::make_unique<Motion>());
      e.addBehavior(std::make_unique<Bounce>());
      e.addBehavior(std::make_unique<RandomSpeed>());
    }
    if(i==1) {
      e.addBehavior(std::make_unique<Motion>());
      e.addBehavior(std::make_unique<Bounce>());
      e.addBehavior(std::make_unique<StartStop>());
    }
    if(i==2) {
      e.addBehavior(std::make_unique<Motion>());
      e.addBehavior(std::make_unique<Through>());
      e.addBehavior(std::make_unique<RandomSpeed>());
    }
    if(i==3) {
      e.addBehavior(std::make_unique<Motion>());
      e.addBehavior(std::make_unique<Through>());
      e.addBehavior(std::make_unique<StartStop>());
    }
    if(i==4) {
      e.addBehavior(std::make_unique<Gravity>());
      e.addBehavior(std::make_unique<Bounce>());
      e.addBehavior(std::make_unique<RandomSpeed>());
    }
    if(i==5) {
      e.addBehavior(std::make_unique<Gravity>());
      e.addBehavior(std::make_unique<Bounce>());
      e.addBehavior(std::make_unique<StartStop>());
    }
    if(i==6) {
      e.addBehavior(std::make_unique<Gravity>());
      e.addBehavior(std::make_unique<Through>());
      e.addBehavior(std::make_unique<RandomSpeed>());
    }
    if(i==7) {
      e.addBehavior(std::make_unique<Gravity>());
      e.addBehavior(std::make_unique<Through>());
      e.addBehavior(std::make_unique<StartStop>());
    }
  }
  win.display();
  return 0;
}
