#include <cmath>
#include <random>
#include "shape.hpp"
#include "window.hpp"

namespace s5loo {

	void Shape::move(const Window &win, double dt) {
		auto [win_width, win_height] = win.size();
		x_ += sx_ * dt;
		y_ += sy_ * dt;

		double bound = boundingSphere();
		
		if (x_ - bound < 0) {
			//x_ = 0;
			sx_= -sx_;//0;
		}
		
		if (x_ + bound >= win_width) {
			x_ = win_width-bound;
			sx_= -sx_;//0;
		}
		
		if (y_ - bound < 0) {
			//y_ = 0; 
			sy_= -sy_;//0;
		}
		
		if (y_ + bound >= win_height) { 
			y_ = win_height-bound; 
			sy_= -sy_;//0;
		}
	}

	void Shape::click(const Window &win, int xMouseClicCrd, int yMouseClicCrd) {
		const double radius = boundingSphere();
		const double dx = (double) xMouseClicCrd - x_;
		const double dy = (double) yMouseClicCrd - y_;

		if (dx*dx + dy*dy <= radius*radius) {
			// dans la sphère englobante
			do_click(win);
		}
	}

	void Shape::do_click(const Window &win) {
		auto [win_width, win_height] = win.size();
		const double windowCenterX = win_width/2;
		const double windowCenterY = win_height/2;

		std::default_random_engine rndGen{std::random_device{}()};
		std::uniform_real_distribution<double> speedDistr{-50, 50};
		double newSpeedX = std::abs(speedDistr(rndGen));
		double newSpeedY = std::abs(speedDistr(rndGen));

		auto [x, y] = position();
		if ((x - windowCenterX) > 0) {
			newSpeedX = -newSpeedX;
		}
		if ((y - windowCenterY) > 0) {
			newSpeedY = -newSpeedY;
		}
		speed(newSpeedX, newSpeedY);
	}

}