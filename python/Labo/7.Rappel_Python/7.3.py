# -*-coding:Latin-1 -*
from math import *

### Bibliothèque de fonctions
def somme(x1, x2):
	return x1+x2

def soustraction(x1, x2):
	return x1-x2

def multiplication(x1, x2):
	return x1*x2

def division(x1,x2):
	return x1/x2

def racine(x):
	return sqrt(x)

def puissance(x, ex):
	return x**ex

def factorielle(x):
	return fact(x)

def valeurAbsolue(x):
	return abs(x)

def cosinus(x):
	return cos(x)

def sinus(x):
	return sin(x)

def moyenne(tab):
	sum = 0
	for i in range(len(tab)):
		sum += tab[i]
	return division(sum, len(tab))

def logarithme(x):
	return log(x)

# question 1
# Ecrire une fonction qui calcul (a - b)²

def fonction1(a, b):
	return puissance(soustraction(a, b), 2)

# question 2

def fonction2(tab):
	x_moy = moyenne(tab)
	result = 0
	for i in range(len(tab)):
		result += fonction1(tab[i], x_moy)
	return result

# question 3

def ecartType(tab):
	return racine((1/len(tab)*fonction2(tab)))

