#include <random>
#include <cmath>
#include "rectangle.hpp"
#include "window.hpp"
namespace s5loo {

	void Rectangle::move(const Window &win, double dt) {
		Shape::move(win, dt);
		angle(angle() + angularSpeed_*dt);
		
		/*
		auto [win_width, win_height] = win.size();
		x_ += sx_ * dt;
		y_ += sy_ * dt;
		
		if (x_ < 0) {
			//x_ = 0;
			sx_= -sx_;//0;
		}
		
		if (x_ + width_ >= win_width) {
			//x_ = width-1-2*radius_;
			sx_= -sx_;//0;
		}
		
		if (y_ < 0) {
			//y_ = 0; 
		}
		
		if (y_ + height_ >= win_height) { 
			//y_ = height-1-2*radius_; 
			sy_= -sy_;//0;
		}*/
	}

	void Rectangle::draw(sf::RenderWindow& win) const {
		auto [x, y] = position();
		auto [rectWidth, rectHeight] = size();
		Color rectColor = color();
		double a = angle();
		
		sf::RectangleShape s{sf::Vector2f{(float)rectWidth, (float)rectHeight}};
		s.setFillColor(sf::Color{rectColor[0], rectColor[1], rectColor[2]});
		s.setOrigin((float)(rectWidth*0.5), (float)(rectHeight*0.5));
		s.setPosition((float)x, (float)y);
		s.setRotation((float)a);		
		win.draw(s);
	}

	void Rectangle::do_click(const Window &win) {
		auto [win_width, win_height] = win.size();
		const double windowCenterX = win_width/2;
		const double windowCenterY = win_height/2;

		std::default_random_engine rndGen{std::random_device{}()};
		std::uniform_real_distribution<double> speedDistr{-50, 50};
		double newSpeedX = std::abs(speedDistr(rndGen));
		double newSpeedY = std::abs(speedDistr(rndGen));

		auto [x, y] = position();
		if ((x - windowCenterX) > 0) {
			newSpeedX = -newSpeedX;
		}
		if ((y - windowCenterY) > 0) {
			newSpeedY = -newSpeedY;
		}
		speed(newSpeedX, newSpeedY);
		
  		std::uniform_real_distribution<double> angularDistr{-30, 30};
		angularSpeed_ = angularDistr(rndGen);
	}
} //end of namespace s5loo