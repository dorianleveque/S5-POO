#
# Class Etudiant regroupant les attribues et les methodes des classes enibien et esiabienne
# 
class Etudiant():
	def __init__(self, name = "undefined"):
		self.__name = name
		print("Creation de " + self.__name)

	def saluer(self):
		print("Bonjour je m'appelle " + self.getName() + " !")

	def getName(self):
		return self.__name

	def __del__(self):
		print("destruction de " + self.getName())

#
# Class Enibien regroupant ses attribues et ses methodes
# 
class Enibien(Etudiant):
	def __init__(self, name = "undefined", semestre = 1):
		Etudiant.__init__(self, name)
		self.__semestre = semestre
		self.__petiteCopine=None

	def afficherPartenaire(self):
		if self.__petiteCopine:
			print self.__petiteCopine
		else:
			print "Dommage !"
	def setPetiteCopine(self, esiabienne):
		self.__petiteCopine = esiabienne

	def suivreSemestre(self):
		self.__semestre = self.__semestre +1


#
# Class Esiabienne regroupant ses attribues et ses methodes
# 
class Esiabienne(Etudiant):
	def __init__(self, name = "undefined", annee = 1):
		Etudiant.__init__(self, name)
		self.__annee = annee
		self.__petitCopain = None

	def sortirAvec(self, enibien):
		self.__petitCopain = enibien
		enibien.setPetiteCopine(self)

	def afficherPartenaire(self):
		print ("moi, "+self.__name+", je sors avec "+self.__petitCopain.__name)

print("Debut du programme")
e1 = Enibien("boby")
e2 = Enibien("Jacques Chi")
e3 = Esiabienne("ginette")
e4 = Esiabienne("Jacqueline Chi")
e1.saluer()
e2.saluer()
e3.sortirAvec(e1)
e1.setPetiteCopine(e3)
e4.sortirAvec(e2)
e1.afficherPartenaire()
e2.afficherPartenaire()
e3.afficherPartenaire()
e4.afficherPartenaire()